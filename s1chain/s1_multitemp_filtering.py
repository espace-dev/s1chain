#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Module for filtering S1 tiled products
"""

import os
import shutil
import subprocess
import logging
from pathlib import Path
import tempfile
import rasterio
import xml.etree.ElementTree as ET
import itertools

from .s1_library import S1Library
from .s1_config import Config

ROOT = Path(os.path.realpath(__file__)).parent.parent
snap_xml_folder = ROOT / "snap_xml"

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


class MultiTempFiltering:
    
    def __init__(self,
                 tile: str = None,
                 pola: str = None,
                 orbi: str = None,
                 first_date: str = '2015-01-01', 
                 last_date: str = '2100-01-01',
                 ) -> None:   
        """All parameters are read in the config.py file. it is possible to force some of them when calling.

        _tile = "tile_id"_ id of S2 tile or shapefile used as extend for S1 tiling
        
        _pola = "vv" or "vh"_ Default is None
        
        _orbi = "DES" or "ASC"_. Default is None.
        
        _ref_date = "YYY-MM-DD"_ date of reference image.
        
        _first_and _last_date = "YYYY-MM-DD"_ are the range of date for tile processing
        """
        self.tile = tile
        logger.info("Tile: {}".format(self.tile))

        self.first_date = first_date
        logger.info("First date: {}".format(self.first_date))
        
        self.last_date = last_date
        logger.info("Last date: {}".format(self.last_date))
        
        self.tiled_lib_path = Path(Config().get("s1_tiled_path"))
        self.filtered_lib_path = Path(Config().get("s1_filtered_path"))
        self.tmp_path = Path(Config().get("tmp_path"))
        self.gpt_path = Path(Config().get("snap_path")) / "bin" / "gpt"
        
        if not pola:
            pola = ['vv', 'vh']
        else:
            pola = [pola]
        if not orbi:
            orbi = ['ASC', 'DES']
        else:
            orbi = [orbi]
        
        for self.pola, self.orbi in list(itertools.product(pola, orbi)):
            logger.info("Polarisation: {}".format(self.pola))
            logger.info("Orbite: {}".format(self.orbi))
            file_list = self.get_files()
            logger.info("Files to process: {}".format(len(file_list)))
            if file_list:
                dim_file = self.stacker_v2(file_list)
                despeckled_file = self.multitemp_despeckle_gpt(dim_file)
                self.split_despeckled_dimap(despeckled_file, file_list)
        
    def get_files(self):
        input_files = getattr(S1Library(), self.tile).tiled.filter_dates(first_date = self.first_date, last_date = self.last_date).\
                                                          filter_attr([self.pola, self.orbi])
        input_paths = [str(self.tiled_lib_path / self.tile / f) for f in input_files]
        return input_paths
    
    def stacker_v2(self, files):
        logger.info('Stacking input files...')
        #~ logger.info(files)
        temp_name = next(tempfile._get_candidate_names())
        out_path_dim = str(self.tmp_path / (temp_name + '.dim'))
        command = [str(self.gpt_path), \
                   str(snap_xml_folder / "01_create_stack.xml"), \
                   '-Pfilelist=' + ",".join(files), \
                   '-Poutfile=' + out_path_dim]
        subprocess.call(command)
        logger.info(('Editing DIM XML...'))
        tree = ET.parse(out_path_dim)
        root = tree.getroot()
        for SBI in root.iter('Spectral_Band_Info'):
            unit = ET.SubElement(SBI, 'PHYSICAL_UNIT')
            unit.text= 'm'
            ET.dump(unit)
        tree.write(out_path_dim)
        return out_path_dim

    def multitemp_despeckle_gpt(self, dim_file):
        out_path_despeckle = str(Path(dim_file).parent / (str(Path(dim_file).stem) + "_filtered.dim"))
        logger.info('Despeckling...')
        command = [str(self.gpt_path), \
                   str(snap_xml_folder / "02_filter.xml"), \
                   '-Pinput=' + dim_file, \
                   '-Poutfile=' + out_path_despeckle]
        subprocess.call(command)
        os.remove(dim_file)
        shutil.rmtree(str(Path(dim_file).parent / (str(Path(dim_file).stem) + ".data")))
        return out_path_despeckle
    
    def split_despeckled_dimap(self, despeckled_file, input_files):
        logger.info(('Spliting despeckled DIM...'))
        dim_folder = Path(despeckled_file).parent / (str(Path(despeckled_file).stem) + ".data")
        glob_files = list(dim_folder.glob("band_*.img"))
        glob_file_name_str = [str(e.stem) for e in glob_files]
        bands = sorted(glob_file_name_str, key=lambda x: int(''.join(c for c in x if c.isdigit())))
        os.makedirs(str(Path(self.filtered_lib_path) / self.tile), exist_ok=True)
        for id, b in enumerate(bands):
            with rasterio.open(str(dim_folder / (b + '.img'))) as src_b:
                meta = src_b.meta
                meta.update(count = 1, 
                            tiled=False,
                            driver="Gtiff")
                out_path = str(Path(self.filtered_lib_path) / self.tile / (str(Path(input_files[id]).stem) + "_filtered.tif"))
                with rasterio.open(out_path, 'w', **meta) as dst:
                    dst.write(src_b.read(1), 1)
        os.remove(despeckled_file)
        shutil.rmtree(str(Path(despeckled_file).parent / (str(Path(despeckled_file).stem) + ".data")))
                    
    
        
