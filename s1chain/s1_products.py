#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Module for managing S1 products
"""

import numpy as np
import rasterio
import logging
from pathlib import Path
from typing import List, Dict, Iterable
import datetime
from .s1_config import Config
import re


logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

class S1Raw:
    raw=12

class S1TiledProduct:
    tiled = 13

class S1FilteredProduct:
    """S1Filtered product class.
    :param identifier: S1Filtered product's identifier or filename.
    """
    def __init__(self,
                 identifier: str = None,
                 ):
        var = re.findall("^s1._(.+)_([a-z][a-z])_([A-Z]{3})_(.{3})_([0-9]{8})t(.{6})_filtered.tif", identifier)
        if var:
            self.identifier = identifier
            self.tile, self.pola, self.orbi, self.trac, self.date, self.time = var[0]
            self.path = Path(Config().get('s1_filtered_path')) / self.tile / self.identifier
        else:
            self.identifier = None
    def __repr__(self):
        return repr(self.identifier)

class S1NDRProduct:
    ndr = 15

class S1NDRWIProduct:
    def __init__(self,
                 identifier: str = None,
                 ):
        #~ s1x_21MXT-Curuai_vh_DES_xxx_20170712-r20171227_NDR-WI.tif
        var = re.findall("^s1._(.+)_([a-z][a-z])_([A-Z]{3})_(.{3})_([0-9]{8})-r(.{8})_NDR-WI.tif", identifier)
        if var:
            self.identifier = identifier
            self.tile, self.pola, self.orbi, self.trac, self.date, self.ref = var[0]
            self.path = Path(Config().get('s1_ndr_path')) / self.tile / ("r" + self.ref) / self.orbi / self.pola / self.identifier
        else:
            self.identifier = None
    def __repr__(self):
        return repr(self.identifier)

class S1WIProduct:
    def __init__(self,
                 identifier: str = None,
                 ):
        #~ s1x_21MXT-Curuai_vv_DES_xxx_20171121_WI.tif
        var = re.findall("^s1._(.+)_([a-z][a-z])_([A-Z]{3})_(.{3})_(.{8})_WI.tif", identifier)
        if var:
            self.identifier = identifier
            self.tile, self.pola, self.orbi, self.trac, self.ref = var[0]
            self.path = Path(Config().get('s1_ndr_path')) / self.tile / ("r" + self.ref) / self.orbi / self.pola / self.identifier
        else:
            self.identifier = None
    def __repr__(self):
        return repr(self.identifier)

