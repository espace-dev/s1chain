# -*- coding: utf-8 -*-

"""
Module for collecting configuration data from ``~/s1chain_data/config/config.cfg``
"""

import os
import logging
from pathlib import Path
from configparser import ConfigParser


logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)

ROOT = Path(os.path.realpath(__file__)).parent.parent

#~ SHARED_DATA = dict(
    #~ qml_500classes = ROOT / "parameters" / "test_CVA3D_500classes_black.qml", 
    #~ qml_9classes = ROOT / "parameters" / "9_classes.qml", 
    #~ qml_11classes = ROOT / "parameters" / "11_classes.qml"
    #~ tiles_index_dict=ROOT / "sen2chain" / "data" / "tiles_index_dict.p",
    #~ peps_download=ROOT / "sen2chain" / "peps_download3.py"
    #~ )


class Config:
    """Class for loading, checking, and retrieving configuration parameters.
    The class reads during its first initialization the configurable settings
    from the configuration file.

    Usage::
        >>> Config().get("l1c_path")
    """
    # TODO: Implement the Config class as a singleton.

    _USER_DIR = Path.home() / "s1chain_data"
    _CONFIG_DIR = _USER_DIR / "config"
    _DEFAULT_DATA_DIR = _USER_DIR / "data"
    _CONFIG_FILE = _CONFIG_DIR / "config.cfg"
    #~ _TILES_TO_WATCH = _CONFIG_DIR / "tiles_to_watch.csv"

    def __init__(self) -> None:

        self._config_params = ConfigParser()
        self._config_params["DATA PATHS"] = {"s1_raw_path": "/DATA_S1/S1_CHAIN/S1_RAW/",
                                             "s1_tiled_path": "/DATA_S1/S1_CHAIN/S1_TILED/",
                                             "s1_filtered_path": "/DATA_S1/S1_CHAIN/S1_FILTERED/",
                                             "s1_ndr_path": "/DATA_S1/S1_CHAIN/S1_NDR/"}
        
        self._config_params["S1TILING CFG"] = {"s1_tiling": "/put/your/S1Tiling/path/here", 
                                               "tile_shapefile_path": "./shapefile/Features.shp",
                                               "srtm_path": "/DATA_S1/S1_CHAIN/S1_DATA/SRTM",
                                               "tmp_path": "/DATA_S1/S1_CHAIN/S1_DATA/tmp",
                                               "download": "False",
                                               "filtering_activated": "False",
                                               }
        
        self._config_params["SNAP PATHS"] = {"snap_path": "/put/your/snap/path/here", 
                                             }
        
        self._config_params["S1CHAIN PARAMETERS"] = {"tile": "40KCB",
                                                     "first_date": "2020-01-01", 
                                                     "last_date": "2020-01-02",
                                                     "ref_date": "put-ref_date-here",
                                                     }

        
        
        #~ self._config_params["HUBS LOGINS"] = {"scihub_id": "",
                                              #~ "scihub_pwd": "",
                                              #~ "peps_config_path": ""}
        
        if self._CONFIG_FILE.exists():
            self._config_params.read(str(self._CONFIG_FILE))
            self._config_params_disk = ConfigParser()
            self._config_params_disk.read(str(self._CONFIG_FILE))
            if self._config_params_disk != self._config_params: 
                self._create_config()
        else:
            self._create_config()
        
        self.config_dict = dict()

        for section in self._config_params.sections():
            for key in self._config_params[section].keys():
                self.config_dict[key] = self._config_params[section][key]

        self._check_data_paths()
        
    def __repr__(self):
        return repr(self.config_dict)

    def _create_config(self) -> None:
        """Create a new config file in ``~/s1chain_data/config/config.cfg``."""
        self._USER_DIR.mkdir(exist_ok=True)
        self._CONFIG_DIR.mkdir(exist_ok=True)
        self._DEFAULT_DATA_DIR.mkdir(exist_ok=True)
        
        with open(str(self._CONFIG_FILE), "w") as cfg_file:
            self._config_params.write(cfg_file)

    def _check_data_paths(self) -> None:
        """
        Checks if data paths are provided and valids. If not, create default
        folders in s1chain_data/data and update the configuration file.
        """
        def update_config(section, key, val):
            """
            Update a setting in config.ini
            """
            self._config_params.set(section, key, val)
            with open(str(self._CONFIG_FILE), "w") as cfg_file:
                self._config_params.write(cfg_file)

        data_paths = [option for option in self._config_params["DATA PATHS"]]

        for path in data_paths:
            value = self.config_dict[path]

            if value.rstrip() == "" or not Path(value).exists():

                default_value = self._DEFAULT_DATA_DIR / path.replace("_path", "").upper()
                default_value.mkdir(parents=True, exist_ok=True)
                update_config("DATA PATHS", path, str(default_value))

                logger.info("{}: using default at {}".format(path, str(default_value)))

    def get(self, param: str) -> str:
        """Returns a parameter value.

        :param param: parameter.
        """
        if param not in self.config_dict:
            raise ValueError("Invalid parameter", param)

        return self.config_dict.get(param)

