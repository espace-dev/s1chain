#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Module for download S1 images and preprocess with S1-Tiling
"""

import configparser
import os
from .s1_config import Config


class DownloadProcess:
    
    def __init__(self,
                 tile: str = None,
                 first_date: str = None,
                 last_date: str = None,
                 download: bool = False,
                 filtering: bool = False,
                 ) -> None:
        """All parameters are filled in the config.py file. it is possible to force some of them when calling the function.

        _tile = "tile_id"_ id of S2 tile or shapefile used as extend for S1 tiling
        
        _first_and _last_date = "YYYY-MM-DD"_ are the range of date for tile processing
        
        _download = True/False_ For download or just process images. Default is False
        
        _filtering = True/Flase_ Used to bypass the S1Tiling Quegan temporal filtering. Must be False. Sen1chain developpers choose Lee temporal filter instead

        """
        self.config_path = os.path.join(Config().get('s1_tiling'),'S1Processor.cfg')
        self.S1Processor_path = os.path.join(Config().get('s1_tiling'),'S1Processor.py')

        self.output_path = Config().get('s1_tiled_path')
        self.raw_path = Config().get('s1_raw_path')
        self.SRTM_path = Config().get('srtm_path')
        self.tmp_path = Config().get('tmp_path')
        
        self.tile = tile or Config().get('tile')
        self.first_date = first_date or Config().get('first_date')
        self.last_date = last_date or Config().get('last_date')
        self.s2grid_shp = Config().get('tile_shapefile_path')
        self.filtering = filtering or Config().get('filtering_activated')
        self.download = download or Config().get('download')
        
    def update_config (self):
        """ Update some information of S1Tiling configuration file based on config.py information"""
        config = configparser.SafeConfigParser()
        config.optionxform = str
        config.read(self.config_path)
        config.set('Paths','Output',self.output_path)
        config.set('Paths','S1Images',self.raw_path)
        config.set('Paths','SRTM',self.SRTM_path)
        config.set('Paths','tmp',self.tmp_path)
        #~ config.set('PEPS','ROI_by_tiles',self.tile)
        config.set('PEPS','Download',self.download)
        config.set('PEPS','first_date',self.first_date)
        config.set('PEPS','last_date',self.last_date)
        config.set('Processing','TilesShapefile',self.s2grid_shp)
        config.set('Processing','Tiles', self.tile)
        config.set('Filtering','Filtering_activated', self.filtering)
        
        config.write(open(str(Config()._CONFIG_DIR / 'S1Processor.cfg'),'w'))
        
    
    def s1tiling(self):
        """Launch S1Tiling; Download images (if activated in the config.py file) and tile them"""
        os.chdir(os.path.dirname(self.S1Processor_path))
        os.system('python '+self.S1Processor_path+' '+ str(Config()._CONFIG_DIR / 'S1Processor.cfg'))
