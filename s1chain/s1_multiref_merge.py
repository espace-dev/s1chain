#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Module for merging NDR from multiple references

This module allows to search all references (WI) and NDR (NDR-WI) masks for a range of date.

The merge processing compute the minimum extent of all WI files and the max extent of NDR masks for each dates availables  
"""

import logging
import os
import rasterio
from rasterio import merge
import numpy as np
import itertools
from pathlib import Path
from .s1_config import Config
from .s1_library import S1Library
from .s1_products import S1NDRWIProduct, S1WIProduct

#~ import matplotlib.pyplot as plt
#~ plt.style.use('seaborn-whitegrid')


logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

class Multiref_Merge:
    def __init__(self,
                 tile: str = None,
                 pola: str = None,
                 orbi: str = None,
                 ref_date: str = None,
                 first_date: str = '2015-01-01', 
                 last_date: str = '2100-01-01'
                 ):
        """All parameters are read in the config.py file. it is possible to force some of them when calling.

        _tile = "tile_id"_ id of S2 tile or shapefile used as extend for S1 tiling
        
        _pola = "vv" or "vh"_ Default is None
        
        _orbi = "DES" or "ASC"_. Default is None.
        
        _ref_date = "YYY-MM-DD"_ date of reference image.
        
        _first_and _last_date = "YYYY-MM-DD"_ are the range of date for tile processing

        """
        self.ndr_lib_path = Path(Config().get("s1_ndr_path"))
        self.tmp_path = Path(Config().get("tmp_path"))

        self.tile = tile
        logger.info("Tile: {}".format(self.tile))
        
        self.ref_date = ref_date
        logger.info("Ref date: {}".format(self.ref_date))
        
        self.first_date = first_date
        logger.info("First date: {}".format(self.first_date))
        
        self.last_date = last_date
        logger.info("Last date: {}".format(self.last_date))
        
        if not pola:
            pola = ['vv', 'vh', 'vx']
        else:
            pola = [pola]
        if not orbi:
            orbi = ['ASC', 'DES']
        else:
            orbi = [orbi]
        
        
        for self.orbi, self.pola in list(itertools.product(orbi, pola)):
            # ~ logger.info("Polarisation: {}".format(self.pola))
            # ~ logger.info("Orbite: {}".format(self.orbi))
            
            self.get_files()
            #~ if self.get_files():
                #~ files = self.get_files()
                #~ self.wi_files = files[0]
                #~ self.ref_files = files[1]
            if self.ndrwi_files and self.wi_files:
                self.merge()
                #~ logger.info("Ref path: {}".format(self.ref_path))
                #~ logger.info("Input paths: {}".format(self.input_paths))
                #~ self.refNDR()
    
    
    #~ def get_files(self):
        #~ ref_file = getattr(S1Library(), self.tile).filtered.filter_dates(first_date = self.ref_date, last_date = self.ref_date).\
                                                          #~ filter_attr([self.pola, self.orbi])
        #~ if ref_file:
            #~ ref_path = [str(self.filtered_lib_path / self.tile / r) for r in ref_file]
            #~ input_files = getattr(S1Library(), self.tile).filtered.filter_dates(first_date = self.first_date, last_date = self.last_date).\
                                                          #~ filter_attr([self.pola, self.orbi])
            #~ input_paths = [str(self.filtered_lib_path / self.tile / f) for f in input_files]
            #~ input_paths = [i for i in input_paths if i not in ref_path]
            #~ if input_paths:
                #~ self.ref_path = ref_path[0]
                #~ self.input_paths = input_paths
                #~ return True
        #~ else:
            #~ return False
            
    
    #~ def get_files(self):
        #~ ndr_tile_path = self.ndr_lib_path / self.tile
        #~ ndr_date_folder_list = [folder for folder in ndr_tile_path.iterdir() if folder.exists() and 'merge' not in folder.name]
        #~ self.wi_files = []
        #~ self.ref_files = []
        #~ for d in ndr_date_folder_list:
            #~ # si exist fichier qui fini par NDR-WI.tif
            #~ print(str(d/self.orbi/self.pola))
            #~ if (d/self.orbi/self.pola).exists():
                 #~ for f in (d/self.orbi/self.pola).iterdir() :
                    #~ if str(f).endswith("NDR-WI.tif"):
                        #~ self.wi_files.append(os.fspath(f))
                    #~ if str(f).endswith("_WI.tif"):
                        #~ self.ref_files.append(os.fspath(f))
    
    def get_files(self):
        """Function to get NDR and reference files return _product_lists_ type into _self.ndrwi_files_ and _self.wi_files_"""
        library = S1Library()
        self.ndrwi_files = getattr(library, self.tile).ndrwi.filter_dates(first_date = self.first_date, last_date = self.last_date).\
                                                          filter_attr([self.pola, self.orbi])
        self.wi_files = getattr(library, self.tile).wi.filter_dates(first_date = self.first_date, last_date = self.last_date).\
                                                          filter_attr([self.pola, self.orbi])
    
    
    def merge (self):
        """Process merging for all NDR products detected for the range of date. Export merged products in a new folder with _"xxxx-xx-xx"_ instead of the reference date."""
        export_path = self.ndr_lib_path / self.tile / 'rxxxxxxxx' / self.orbi / self.pola
        try:
            os.makedirs(str(export_path))
        except:
            pass
        #~ img_date_list = list(set([str(file_name.split('_')[6])[:8] for file_name in self.ndrwi_files]))
        img_date_list = list(set([S1NDRWIProduct(file).date for file in self.ndrwi_files]))
        logger.info("merge_list: {}".format(self.ndrwi_files))
        logger.info("img_date_list: {}".format(img_date_list))

        for date in img_date_list:
            merge_list_ndrwi = []
            logger.info(" Merging date : {}".format(date))
            for f in self.ndrwi_files:
                f_product = S1NDRWIProduct(f)
                if f_product.date == date:
                    profile = rasterio.open(f_product.path).profile
                    merge_list_ndrwi.append(rasterio.open(f_product.path).read(1))
            merge_wi = np.amax(merge_list_ndrwi,axis=0)
            with rasterio.open(str(export_path)+'/s1x_'+ self.tile+'_'+self.pola+'_'+self.orbi+'_xxx_'+str(date)+'-rxxxxxxxx_NDR-WI.tif', 'w',**profile) as r:
                r.write(merge_wi,1)
        
        merge_list_wi = []
        logger.info(" Merging ref files : {}".format(self.wi_files))
        
        profile = rasterio.open(S1WIProduct(list(self.wi_files)[0]).path).profile
        merge_list_wi = [rasterio.open(S1WIProduct(ref).path).read(1) for ref in self.wi_files]
        merge_ref = np.amin(merge_list_wi,axis=0)
        with rasterio.open(str(export_path)+'/s1x_'+ self.tile+'_'+self.pola+'_'+self.orbi+'_xxx_xxxxxxxx_WI.tif', 'w',**profile) as r:
                        r.write(merge_ref,1)
            
        

