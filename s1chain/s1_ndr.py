#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
    Module to process Normalized Difference Ratio
    from list of images
"""

import logging
import os
import rasterio
from rasterio import merge
import numpy as np
from scipy import ndimage
import itertools
from pathlib import Path
#~ from skimage import data
#~ from skimage import filters
# library histogram
from rasterio import plot
from rasterio.plot import show_hist
from matplotlib import pyplot
#~ import matplotlib.pyplot as plt
#~ plt.style.use('seaborn-whitegrid')


from .s1_config import Config
from .s1_library import S1Library
from .s1_products import S1FilteredProduct

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

class NDRProcessing:
    def __init__(self,
                 tile: str = None,
                 pola: str = None,
                 orbi: str = None,
                 ref_date: str = None,
                 first_date: str = '2015-01-01', 
                 last_date: str = '2100-01-01',
                 merge: bool = False,
                 filter_maj: int = None,
                 #~ path_list: list = None,
                 ):
        """All parameters are read in the config.py file. it is possible to force some of them when calling.

        _tile = "tile_id"_ id of S2 tile or shapefile used as extend for S1 tiling
        
        _pola = "vv" or "vh"_ Default is None
        
        _orbi = "DES" or "ASC"_. Default is None.
        
        _ref_date = "YYY-MM-DD"_ date of reference image.
        
        _first_and _last_date = "YYYY-MM-DD"_ are the range of date for tile processing
        
        _merge = True/False_. Polarization merging. Default is False.
        
        _filter_maj = int_. Majority filter parmaeter. Default is 3.
        """
        self.filtered_lib_path = Path(Config().get("s1_filtered_path"))
        self.tmp_path = Path(Config().get("tmp_path"))

        self.tile = tile
        logger.info("Tile: {}".format(self.tile))
        
        self.ref_date = ref_date
        logger.info("Ref date: {}".format(self.ref_date))
        
        self.first_date = first_date
        logger.info("First date: {}".format(self.first_date))
        
        self.last_date = last_date
        logger.info("Last date: {}".format(self.last_date))
        
        self.filter_maj = filter_maj
        logger.info("filter_maj: {}".format(self.filter_maj))
        
        if not pola:
            pola = ['vv', 'vh']
        else:
            pola = [pola]
        if not orbi:
            orbi = ['ASC', 'DES']
        else:
            orbi = [orbi]
        
        for self.pola, self.orbi in list(itertools.product(pola, orbi)):
            logger.info("Orbite: {}".format(self.orbi))
            logger.info("Polarisation: {}".format(self.pola))

            if self.get_files():
                #~ self.get_files()
                logger.info("Ref: {}".format(self.ref))
                logger.info("Inputs: {}".format(self.inputs))
                self.ref_NDR()
        
        if merge:
            for self.orbi in orbi:
                self.merge_NDR_pola()
        
        #~ self.histogram()
            
    def get_files(self):
        """Method to get tiles corresponding to the paramters of config file"""
        ref = getattr(S1Library(), self.tile).filtered.filter_dates(first_date = self.ref_date, last_date = self.ref_date).\
                                                          filter_attr([self.pola, self.orbi])
        if ref:
            inputs = getattr(S1Library(), self.tile).filtered.filter_dates(first_date = self.first_date, last_date = self.last_date).\
                                                          filter_attr([self.pola, self.orbi])
            if inputs:
                self.ref = list(ref)[0]
                self.inputs = inputs
                return True
        else:
            return False
                
    def ref_NDR (self):
        """ Method to process NDR from a reference"""
        out_path = os.path.join(Config().get('s1_ndr_path'), self.tile, "r" + self.ref_date.replace("-", ""), self.orbi, self.pola)
        try:
            os.makedirs(out_path)
        except:
            pass
        ref_product = S1FilteredProduct(self.ref)
        for img in self.inputs:
                logger.info("Processing: {}".format(img))
                img_product = S1FilteredProduct(img)
                if img_product.date != ref_product.date:
                    r_before = rasterio.open(ref_product.path)
                    r_before_band = r_before.read(1)
                    msk_before = r_before.read_masks(1)
                    r_before_date = ref_product.date
                    r_after_date = img_product.date
                    
                    out_path_ndr = out_path + '/' + 's1x_' + self.tile + '_' + self.pola + '_' + self.orbi + '_xxx_' + r_after_date + '-r' + r_before_date + '_NDR.tif'
                    out_path_ndrwi = out_path + '/' + 's1x_' + self.tile + '_' + self.pola + '_' + self.orbi + '_xxx_' + r_after_date + '-r' + r_before_date + '_NDR-WI.tif'
                    out_path_ref_thsld = out_path + '/' + 's1x_' + self.tile + '_' + self.pola + '_' + self.orbi + '_xxx_' + r_before_date + '_WI.tif'

                    if (not os.path.exists(out_path_ndr)) or (not os.path.exists(out_path_ndrwi)):
                        
                        if self.pola == 'vh':
                            #~ thsld_val_filtered = 0.005 # limite pas assez idem pour 0.006
                            thsld_val_filtered = 0.006
                            #~ thsld_val_filtered = 0.01 # trop
                            tampon = 0.002
                            #~ thsld_val_ndr = -2500
                            thsld_val_ndr = -3000
    
                        elif self.pola == 'vv':
                            #~ thsld_val_filtered = 0.02    # pas mal, peut être pas assez
                            thsld_val_filtered = 0.025
                            #~ tampon = 0.1 # c'est comme si y'avait rien ici pour tester
                            tampon = 0.005 # 
                            thsld_val_ndr = -3500
                            
                        r_after = rasterio.open(img_product.path)
                        r_after_band = r_after.read(1)
                        msk_after = r_after.read_masks(1)
                        r_meta = r_after.profile
                        r_mask_meta = r_after.profile
                        r_meta.update(nodata=32767,
                                      dtype = rasterio.int16,
                                      compress = 'lzw'
                                      )
                        r_mask_meta.update(nodata = 0,
                                           dtype = rasterio.int8,
                                           compress = 'lzw'
                                           )
                        
                        #~ std_after = np.std(r_after_band)
                        #~ mean_after = np.mean(r_after_band)
                        #~ std_before = np.std(r_before_band)
                        #~ mean_before = np.mean(r_before_band)
                        #~ logger.info("std_after: {}".format(std_after))
                        #~ logger.info("mean_after: {}".format(mean_after))
                        #~ logger.info("std_before: {}".format(std_before))
                        #~ logger.info("mean_before: {}".format(mean_before))
    
                        logger.info("Processing NDR ...")
                        NDR = np.empty(r_before.shape, dtype=rasterio.float32)
                        check = np.logical_and ( r_before_band != 0.0, r_after_band != 0.0)
                        NDR = np.where (check, ((r_after_band-r_before_band)/(r_after_band+r_before_band))*10000, 32767)
                        check_nan = np.logical_and(msk_after == 255,msk_before == 255)
                        NDR = np.where(check_nan, NDR, 32767)
                        #~ logger.info("NDR : Done")
                        
                        if not os.path.exists(out_path_ndr):
                            with rasterio.open(out_path_ndr, 'w', **r_meta) as rndr:
                                rndr.write(NDR.astype(rasterio.int16), 1)
                        else:
                            logger.info("NDR already saved, skipping...")
                        
                        logger.info("Processing water impact ...")
                        #~ check = NDR <= -2000
                        #~ check = np.logical_and(NDR <= -3000, r_after_band < min(0.08,filters.threshold_otsu(r_after_band)))
                        #~ NDR_WI = np.where(check, 1, 0)
                        #~ NDR_WI = NDR < thsld_val_ndr
                        #~ NDR_WI = np.logical_and(r_after_band < thsld_val_filtered, r_before_band > thsld_val_filtered)
                        NDR_WI = (NDR < thsld_val_ndr) * (r_after_band < (thsld_val_filtered+tampon)) * (r_before_band > (thsld_val_filtered-tampon))
                        
                        if self.filter_maj:
                            NDR_WI_MAJ = ndimage.median_filter(NDR_WI, size = (self.filter_maj, self.filter_maj))
                            NDR_WI = NDR_WI_MAJ
                        
                        with rasterio.open(out_path_ndrwi, 'w', **r_mask_meta) as rwi:
                            rwi.write(NDR_WI.astype(rasterio.int8), 1)
                        #~ logger.info("Export WI : Done")
                        
                        if not os.path.exists(out_path_ref_thsld):
                            #~ logger.info("Mean-ET {}".format(mean_before-(1.5*std_before)))
                            #logger.info("Otsu {}".format(filters.threshold_otsu(r_before_band)))
                            #~ thsld = np.where(r_before_band < mean_before-(1.5*std_before),1,0)
                            thsld = np.where(r_before_band < thsld_val_filtered,1,0)
                            
                            if self.filter_maj:
                                thsld_MAJ = ndimage.median_filter(thsld, size = (self.filter_maj, self.filter_maj))
                                thsld = thsld_MAJ
                            
                            with rasterio.open(out_path_ref_thsld, 'w', **r_mask_meta) as r_thsld:
                                r_thsld.write(thsld.astype(rasterio.int8), 1)
                            #~ logger.info("Ref background : Done")
                    else:
                        logger.info("NDR already computed")

            
    def merge_NDR_pola (self):
        """ Method to merge NDR computed in vv and vh, creating a 'vx' folder for outputs"""
        logger.info("Merging: {}".format(self.orbi))
        orbi_path = os.path.join(Config().get('s1_ndr_path'), self.tile, "r" + self.ref_date.replace("-", ""), self.orbi)
        if os.path.exists(orbi_path):
            logger.info("Search in folder: {}".format(orbi_path))
            try:
                os.makedirs(os.path.join(orbi_path,'vx'))
            except:
                pass
            
            for img_vv in os.listdir(os.path.join(orbi_path,'vv')):
                if ('WI' in img_vv) and ('.xml' not in img_vv) and ('stats' not in img_vv):
                    logger.info("VV_NDR: {}".format(img_vv))
                    img_vh = img_vv.replace('vv','vh') 
                    
                    if os.path.isfile(orbi_path+ '/vh/' + img_vh):
                        logger.info("Merging: {} + {}".format(img_vv, img_vh))
                        vv = rasterio.open(orbi_path + '/vv/' + img_vv, 'r')
                        vh = rasterio.open(orbi_path + '/vh/' + img_vh, 'r')
                        profile = vv.profile
                        profile.update(nodata = 0,
                                       dtype = rasterio.int8,
                                       count=1,
                                       compress = 'lzw'
                                       )
                        merge = rasterio.merge.merge([vv,vh])
                      
                        merge[0].shape = merge[0].shape[1:]
                        
                        with rasterio.open(orbi_path + '/vx/' + img_vv.replace('vv', 'vx'), 'w', **profile) as r:
                            r.write(merge[0],1)
                        logger.info("Merge done")     
                    else:
                        logger.info("Nothing to merge")
        else:
            logger.info("No products")


    #~ def histogram(self):
               
        #~ out_path_ndr = os.path.join(Config().get('s1_ndr_path'), self.tile, self.ref_date, self.orbi, self.pola)
        #~ input_files = getattr(S1Library(), self.tile).filtered.filter_dates(first_date = self.first_date, last_date = self.last_date).\
                                                          #~ filter_attr([self.pola, self.orbi])
        
        #~ fig = pyplot.figure()
        #~ for img in input_files:
            #~ if ('filtered.tif' in img) :
                #~ r_file = rasterio.open(os.path.join(out_path_ndr,img))
                #~ r = r_file.read(1)
                              
                #~ hist = np.histogram(r,np.arange(0,0.5,0.05))
                #~ logger.info(img)
                #~ print(hist)
                #~ pyplot.plot(np.arange(0,0.45,0.05),hist[0])
        #~ pyplot.show()
