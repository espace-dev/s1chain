#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Module for computing flood frequency on a list of tiles.
"""

import logging
import os
import rasterio
from rasterio.windows import Window
import numpy as np
import itertools
from pathlib import Path
from .s1_config import Config
from .s1_library import S1Library
from .s1_products import S1NDRWIProduct

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

class NDRStats:
    def __init__(self,
                 tile: str = None,
                 pola: str = None,
                 orbi: str = None,
                 ref_date: str = None,
                 first_date: str = '2015-01-01', 
                 last_date: str = '2100-01-01',
                 #~ path_list: list = None,
                 ):
        """All parameters are read in the config.py file. it is possible to force some of them when calling.

        _tile = "tile_id"_ id of S2 tile or shapefile used as extend for S1 tiling
        
        _pola = "vv" or "vh"_ Default is None
        
        _orbi = "DES" or "ASC"_. Default is None.
        
        _ref_date = "YYY-MM-DD"_ date of reference image.
        
        _first_and _last_date = "YYYY-MM-DD"_ are the range of date for tile processing
        """
        self.ndr_lib_path = Path(Config().get("s1_ndr_path"))
        #~ self.tmp_path = Path(Config().get("tmp_path"))

        self.tile = tile
        logger.info("Tile: {}".format(self.tile))
        
        self.ref_date = ref_date
        logger.info("Ref date: {}".format(self.ref_date))
        
        self.first_date = first_date
        logger.info("First date: {}".format(self.first_date))
        
        self.last_date = last_date
        logger.info("Last date: {}".format(self.last_date))
        
        if not orbi:
            orbi = ['ASC', 'DES']
        else:
            orbi = [orbi]
        if not pola:
            pola = ['vv', 'vh', 'vx']
        else:
            pola = [pola]
        
        for self.pola, self.orbi in list(itertools.product(pola, orbi)):
            if self.ref_date == 'xxxx-xx-xx':
                self.inputs = getattr(S1Library(), self.tile).merged_ndrwi.filter_dates(first_date = self.first_date, last_date = self.last_date).\
                                                      filter_attr([self.pola, self.orbi, "r" + self.ref_date.replace("-", "")])
            else:
                self.inputs = getattr(S1Library(), self.tile).ndrwi.filter_dates(first_date = self.first_date, last_date = self.last_date).\
                                                      filter_attr([self.pola, self.orbi, "r" + self.ref_date.replace("-", "")])
            logger.info("Orbite / Polarisation / Products to process: {} / {} / {}".format(self.orbi, self.pola, len(self.inputs)))
            #~ logger.info(self.inputs)
            if self.inputs:
                self.outpath = self.ndr_lib_path / self.tile / ("r" + self.ref_date.replace("-", "")) / self.orbi / self.pola 
                self.process_stats()
                
            
    def process_stats(self):
        """Method for flood fequency computing. Output is tiff file. 
        The value of each pixels represent the count of flooded images 
        in persentage of the total time period.""" 
        srcs = []
        for ndrwi in self.inputs:
            ndrwi_prod = S1NDRWIProduct(ndrwi)
            srcs.append(rasterio.open(ndrwi_prod.path))
        image_profile = srcs[0].profile
        result_cube = np.zeros((1, image_profile['height'], image_profile['width']), dtype=np.int8)
        result_cube_sum = np.zeros((1, image_profile['height'], image_profile['width']), dtype=np.int8)
            
        for row in range(0, image_profile['height']-1, 10):
            nrows = min(image_profile['height'] - row, 10)
            slice_cube = np.zeros((len(self.inputs), nrows, image_profile['width']), dtype=np.int8)
            for id, src in enumerate(srcs):            
                slice_cube[id] = src.read(1, window=Window(0, row, image_profile['width'], nrows))
            
            slice_cube = np.where(slice_cube == 0, np.nan, slice_cube)
            np.nanmax(slice_cube, axis=0, out=result_cube[0, row:row+10])
            np.nansum(slice_cube, axis=0, out=result_cube_sum[0, row:row+10])
            
        image_profile.update(driver="Gtiff",
                             compress="deflate",
                             tiled=False,
                             dtype=np.int8,
                             nodata=0,
                             transform=srcs[0].transform,
                             count=1)
        image_profile.pop('tiled', None)
        
        outpath_tif = self.outpath / ('s1x_' + self.tile + '_' + self.pola + '_' + self.orbi + '_xxx_'+ \
                                      os.path.commonprefix([self.first_date, self.last_date]).replace('-', '').ljust(8, 'x') + '-r' + self.ref_date.replace('-', '') + \
                                      '_NDR-WI-stats-Max.tif')
        outpath_sum = self.outpath / ('s1x_' + self.tile + '_' + self.pola + '_' + self.orbi +  '_xxx_'+ \
                                      os.path.commonprefix([self.first_date, self.last_date]).replace('-', '').ljust(8, 'x') + '-r' + self.ref_date.replace('-', '') + \
                                      '_NDR-WI-stats-Pct.tif')
        outpath_tif.parent.mkdir(parents = True, exist_ok = True)
        
        with rasterio.Env(GDAL_CACHEMAX=512) as env:
            with rasterio.open(str(outpath_tif), "w", **image_profile) as dst:
                dst.write(result_cube[0].astype(np.int8), 1)
            image_profile.update(dtype=np.float32)
            with rasterio.open(str(outpath_sum), "w", **image_profile) as dst:
                dst.write(result_cube_sum[0].astype(np.float32)/len(self.inputs), 1)
        for ob in srcs:
            ob.close()
