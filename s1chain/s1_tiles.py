#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Module for managing S1 tiles
"""

import os
import numpy as np
import rasterio
from matplotlib import pyplot
import logging
from pathlib import Path
from typing import List, Dict, Iterable
import datetime
from .s1_config import Config
from .s1_library import S1Library
from .s1_download_process import DownloadProcess
from .s1_flooded_surface import FloodedArea
from .s1_multitemp_filtering import MultiTempFiltering
from .s1_ndr import NDRProcessing
from .s1_ndr_stats import NDRStats
from .s1_multiref_merge import Multiref_Merge
from .s1_products import S1FilteredProduct

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

class S1Tile:

    def __init__(self,
                 ) -> None:
        
        self.tile = Config().get('tile')
        self.ref_date = Config().get('ref_date')
        self.first_date = Config().get('first_date')
        self.last_date = Config().get('last_date')
        
        logger.info("Tile: {}".format(self.tile))
        logger.info("First - last dates: {} - {}".format(Config().get('first_date'), Config().get('last_date')))
        logger.info("S1Tiling download: {}".format(Config().get('download')))
        logger.info("S1Tiling Filtering_activated: {}".format(Config().get('filtering_activated')))
        logger.info("Ref date: {}".format(Config().get('ref_date')))
        
    @property
    def tiled(self):
        tiled_list = getattr(S1Library(), self.tile).tiled
        logger.info('{} tiled products'.format(len(tiled_list)))
        return tiled_list
    
    @property
    def filtered(self):
        filtered_list = getattr(S1Library(), self.tile).filtered
        logger.info('{} tiled products'.format(len(filtered_list)))
        return filtered_list
    
    @property
    def ndr(self):
        return getattr(S1Library(), self.tile)
    
    def downloadprocess(self,
                        ) -> None:

        dl_instruction = DownloadProcess()
        dl_instruction.update_config()
        dl_instruction.s1tiling()

    def filtering(self,
                 pola: str = None,
                 orbi: str = None,
                 first_date: str = None,
                 last_date: str = None,
                 ) -> None:
        
        MultiTempFiltering(tile = self.tile,
                           pola = pola,
                           orbi = orbi,
                           first_date = first_date or self.first_date, 
                           last_date = last_date or self.last_date)

    def process_ndr(self,
                 pola: str = None,
                 orbi: str = None,    
                 ref_date: str = None,
                 first_date: str = None,
                 last_date: str = None,
                 merge: bool = False,
                 filter_maj: int = 3,
                 ) -> None:
                     
        NDRProcessing(tile = self.tile,
                      pola = pola,
                      orbi = orbi,
                      merge = merge,
                      filter_maj = filter_maj,
                      ref_date = ref_date or self.ref_date,
                      first_date = first_date or self.first_date, 
                      last_date = last_date or self.last_date)
    
    def process_ndr_multiref(self,
                 pola: str = None,
                 orbi: str = None,    
                 ref_date: str = None,
                 first_date: str = None,
                 last_date: str = None,
                 merge: int = None,
                 filter_maj = 3,
                 ) -> None:
        
        first_date = first_date or self.first_date
        last_date = last_date or self.last_date
        ref_date = ref_date or self.ref_date

        ref_dates = sorted(list(set([S1FilteredProduct(filtered).date for filtered in getattr(S1Library(), self.tile).\
                                                                                      filtered.\
                                                                                      filter_dates(first_date = first_date, last_date = last_date)\
                                                                      #~ if S1FilteredProduct(filtered).date != ref_date.replace("-", "")\
                                    ])))
        logger.info(ref_dates)
        
        for ref_date in ref_dates:
            ref_date = datetime.datetime.strptime(ref_date, '%Y%m%d').strftime('%Y-%m-%d')
            NDRProcessing(tile = self.tile,
                          pola = pola,
                          orbi = orbi,
                          merge = merge,
                          filter_maj = filter_maj,
                          ref_date = ref_date,
                          first_date = first_date, 
                          last_date = last_date)
    
    def process_ndr_stats(self,
                          pola: str = None,
                          orbi: str = None,  
                          ref_date: str = None,
                          first_date: str = None,
                          last_date: str = None,
                          ) -> None:
                              
        NDRStats(tile = self.tile,
                      pola = pola,
                      orbi = orbi,
                      ref_date = ref_date or self.ref_date,
                      first_date = first_date or self.first_date, 
                      last_date = last_date or self.last_date)
                      
    def compute_flooded_area(self,
                          pola: str = None,
                          orbi: str = None,  
                          ref_date: str = None,
                          first_date: str = None,
                          last_date: str = None,
                          ) -> None:
                              
        FloodedArea(tile = self.tile,
                      pola = pola,
                      orbi = orbi,
                      ref_date = ref_date or self.ref_date,
                      first_date = first_date or self.first_date, 
                      last_date = last_date or self.last_date)
    
    def histogram(self,
                  pola: str = None,
                  orbi: str = None,  
                  first_date: str = None,
                  last_date: str = None,
                  ) -> None:
        
        self.first_date = first_date or self.first_date 
        self.last_date = last_date or self.last_date
               
        filtered_path = os.path.join(Config().get('s1_filtered_path'), self.tile)
        input_files = getattr(S1Library(), self.tile).filtered.filter_dates(first_date = self.first_date, last_date = self.last_date).\
                                                          filter_attr([pola, orbi])
        
        fig = pyplot.figure()
        for img in input_files:
            if ('filtered.tif' in img) :
                r_file = rasterio.open(os.path.join(filtered_path,img))
                r = r_file.read(1)
                hist = np.histogram(r,np.arange(0,0.1,0.001))
                logger.info(img)
                #~ print(hist)
                pyplot.plot(np.arange(0,0.099,0.001),hist[0])
        pyplot.show()
        
    def multiref_merge(self,
                 pola: str = None,
                 orbi: str = None,    
                 ref_date: str = None,
                 first_date: str = None,
                 last_date: str = None
                 ) -> None:
                     
        Multiref_Merge(tile = self.tile,
                      pola = pola,
                      orbi = orbi,
                      ref_date = ref_date or self.ref_date,
                      first_date = first_date or self.first_date, 
                      last_date = last_date or self.last_date)
