#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Module for listing S1 products
"""

import numpy as np
import rasterio
import logging
from pathlib import Path
from typing import List, Dict, Iterable
import datetime
from .s1_config import Config


logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

class ProductList:
    
    def __init__(self,
                 prod_list,
                 tile):
        self._prod_list = sorted(prod_list, key=lambda x:x[16+len(tile):])
        self.tile = tile
        #~ self._order_list
    
    def __repr__(self):
        return repr(self._prod_list)
        
    def __len__(self) -> int:
        return len(self._prod_list)
        
    def __iter__(self) -> Iterable:
        return (self[k] for k in self._prod_list)
    
    def __contains__(self, item) -> bool:
        return item in self._prod_list
        
    def __getitem__(self, item):
        if item in self._prod_list:
            return item
    
    @property
    def filtered(self):
        return ProductList([item for item in self._prod_list if 'filtered' in item], self.tile)
        
    @property
    def tiled(self):
        return ProductList([item for item in self._prod_list \
                            if 'filtered' not in item \
                            if 'NDR' not in item \
                            if 'WI' not in item]\
                            , self.tile)
    
    @property
    def ndr(self):
        return ProductList([item for item in self._prod_list if 'NDR.' in item if 'stats' not in item], self.tile)

    @property
    def ndrwi(self):
        return ProductList([item for item in self._prod_list if 'NDR-WI' in item if (('stats' not in item) and ('xxxxxxxx' not in item))], self.tile)
        
    @property
    def wi(self):
        return ProductList([item for item in self._prod_list if '_WI' in item if (('stats' not in item) and ('xxxxxxxx' not in item))], self.tile)
        
    @property
    def merged_ndrwi(self):
        return ProductList([item for item in self._prod_list if 'NDR-WI' in item if (('stats' not in item) and ('xxxxxxxx' in item))], self.tile)
        
    @property
    def merged_wi(self):
        return ProductList([item for item in self._prod_list if '_WI' in item if (('stats' not in item) and ('xxxxxxxx' in item))], self.tile)
    
    def filter_attr(self, filter_list):
        return ProductList([item for item in self._prod_list if all(orb in item for orb in filter_list)], self.tile)
    
    def filter_dates(self, first_date, last_date):
        first_datetime = datetime.datetime.strptime(first_date, '%Y-%m-%d')
        last_datetime = datetime.datetime.strptime(last_date, '%Y-%m-%d')
        return ProductList([item for item in self._prod_list if \
            first_datetime <= datetime.datetime.strptime(item[16+len(self.tile):24+len(self.tile)], '%Y%m%d') <= last_datetime], self.tile)
    
    # Use filter_nodata only after tiled function
    def filter_nodata(self,rate):
        tiled_lib_path = Path(Config().get("s1_tiled_path"))
        list_raster = []
        for item in self._prod_list:
            raster = rasterio.open(str(tiled_lib_path/item[4:9]/item))
            mask = raster.read_masks(out_shape=(1,int(raster.height/100),int(raster.width/100)))
            m_array = np.array(mask)
            if (np.count_nonzero(m_array==0)/m_array.size)*100 <= 100-rate:
                list_raster.append(item)
        return ProductList([item for item in list_raster], self.tile) 

    #~ def order_list(self):
        #~ return ProductList(self._prod_list.sort(key=lamdba x: x[-21]))
    #~ sorted(s1chain.S1Library()._17RQK._prod_list, key=lambda x:x[21:])

class S1Library:
    
    def __init__(self,
                 tiled_lib_path: Path = Path(Config().get("s1_tiled_path")),
                 filtered_lib_path: Path = Path(Config().get("s1_filtered_path")),
                 ndr_lib_path: Path = Path(Config().get("s1_ndr_path")),
                 ) -> None:   
        
        self._tpath = tiled_lib_path
        self._fpath = filtered_lib_path
        self._npath = ndr_lib_path
        
        
        
        self.tiles = {'tiled': [f.name for f in self._tpath.glob("*")],
                      'filtered': [f.name for f in self._fpath.glob("*")],
                      'ndr': [f.name for f in self._npath.glob("*")]}
        
        for tile in set().union(*self.tiles.values()):
            self.__dict__[tile] = []
            self.__dict__["_" + tile] = []
            for f in ([a for a in ((self._tpath / tile).glob("s1*_*.tif")) if "BorderMask" not in str(a)] + \
                      [a for a in ((self._fpath / tile).glob("s1*_*.tif")) if "BorderMask" not in str(a)] + \
                      [a for a in ((self._npath / tile).glob("r*/*/*/*_NDR-WI.tif")) if "BorderMask" not in str(a)] + \
                      [a for a in ((self._npath / tile).glob("r*/*/*/*_WI.tif")) if "BorderMask" not in str(a)]):
                self.__dict__[tile] += [f.name]
                self.__dict__["_" + tile] += [f.name]
            self.__dict__[tile] = ProductList(self.__dict__[tile], tile)
            self.__dict__["_" + tile] = ProductList(self.__dict__["_" + tile], tile)
                   



