#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Module for computing flooded area on NDR time serie

"""

import csv
from datetime import datetime as dt
import geopandas as gpd
import logging
import os
import rasterio
from rasterio import crs
from rasterio.windows import Window
from rasterstats import zonal_stats
import numpy as np
import itertools
from pathlib import Path
from .s1_config import Config
from .s1_library import S1Library
from .s1_products import S1NDRWIProduct
from .s1_products import S1WIProduct

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

class FloodedArea:
    def __init__(self,
                 tile: str = None,
                 pola: str = None,
                 orbi: str = None,
                 ref_date: str = None,
                 first_date: str = '2015-01-01', 
                 last_date: str = '2100-01-01',
                 #~ path_list: list = None,
                 ):
        """All parameters are read in the config.py file. it is possible to force some of them when calling.

        _tile = "tile_id"_ id of S2 tile or shapefile used as extend for S1 tiling
        
        _pola = "vv" or "vh"_ Default is None
        
        _orbi = "DES" or "ASC"_. Default is None.
        
        _ref_date = "YYY-MM-DD"_ date of reference image.
        
        _first_and _last_date = "YYYY-MM-DD"_ are the range of date for tile processing
        """

        self.ndr_lib_path = Path(Config().get("s1_ndr_path"))
        
        self.shape_path = Path(Config().get("tile_shapefile_path"))
       
        self.tile = tile
        logger.info("Tile: {}".format(self.tile))
        
        self.ref_date = ref_date
        logger.info("Ref date: {}".format(self.ref_date))
        
        self.first_date = first_date
        logger.info("First date: {}".format(self.first_date))
        
        self.last_date = last_date
        logger.info("Last date: {}".format(self.last_date))
        
        if not orbi:
            orbi = ['ASC', 'DES']
        else:
            orbi = [orbi]
        if not pola:
            pola = ['vv', 'vh', 'vx']
        else:
            pola = [pola]
        
        for self.pola, self.orbi in list(itertools.product(pola, orbi)):
            if self.ref_date == 'xxxx-xx-xx':
                self.inputs = getattr(S1Library(), self.tile).merged_ndrwi.filter_dates(first_date = self.first_date, last_date = self.last_date).\
                                                      filter_attr([self.pola, self.orbi, "r" + self.ref_date.replace("-", "")])
                self.ref_input = getattr(S1Library(), self.tile).merged_wi.filter_attr([self.pola, self.orbi,self.ref_date.replace("-", "")])
            else:
                self.inputs = getattr(S1Library(), self.tile).ndrwi.filter_dates(first_date = self.first_date, last_date = self.last_date).\
                                                      filter_attr([self.pola, self.orbi, "r" + self.ref_date.replace("-", "")])
                self.ref_input = getattr(S1Library(), self.tile).wi.filter_attr([self.pola, self.orbi,self.ref_date.replace("-", "")])
            logger.info("Orbite / Polarisation / Products to process: {} / {} / {}".format(self.orbi, self.pola, len(self.inputs)))
            if self.inputs:
                self.outpath = self.ndr_lib_path / self.tile / ("r" + self.ref_date.replace("-", "")) / self.orbi / self.pola 
                self.compute_flooded_area()
            
    def compute_flooded_area(self):
        #~ print(self.ref_input)
        srcs = []
        surface = []
        for ndrwi in self.inputs:
            ndrwi_prod = S1NDRWIProduct(ndrwi)
            srcs.append(rasterio.open(ndrwi_prod.path))
        image_crs = srcs[0].crs
        logger.info("Image EPSG : {}".format(image_crs))
        logger.info("Reading : {}".format(self.shape_path))
        shape = gpd.read_file(self.shape_path)
        logger.info("Reprojecting...")
        reproj_shape = shape.to_crs(image_crs)
        logger.info("Done")
        
        logger.info("Calculating flooded surface...")
        for ref_wi in self.ref_input:
            ref_prod = S1WIProduct(ref_wi)
            ref = rasterio.open(ref_prod.path)
            
        ref_surface = zonal_stats(reproj_shape, ref.name,stats = 'count')
        ref_surface = (ref_surface[0]['count'])/10000
        
        for wi in srcs:
            stats = zonal_stats(reproj_shape,wi.name,stats = 'count')
            date = dt.strptime(wi.name[-29:-21], '%Y%m%d')
            # data = dt.strftime("%d/%m/%y")
            surface.append([date.strftime('%d/%m/%y'),((stats[0]['count'])/10000)+ref_surface])
        logger.info("Flooded surface done, saving...")
        
        with open(os.path.join(str(self.outpath),"s1x_" + self.tile + "_" + self.pola + "_" + self.orbi + "_xxx_r" + self.ref_date.replace("-", "") + "_flooded_surface.csv"), 'w', newline='') as myfile:
            wr = csv.writer(myfile)
            wr.writerows(surface)
        logger.info("CSV saved")
