#!/usr/bin/python3
# !#-*-coding:utf-8-*-

from setuptools import setup, find_packages
from os import path

here = path.abspath(path.dirname(__file__))

with open("s1chain/__init__.py", encoding="utf-8") as f:
    for line in f:
        if line.startswith("__version__"):
            version = line.split("=")[1].strip().strip("\"")

with open(path.join(here, "README.md"), encoding="utf-8") as f:
    long_description = f.read()

setup(
    name="s1chain",
    version=version,
    description="RenovRisk Impact Sentinel 1 - Change Detection",
    long_description=long_description,
    long_description_content_type="text/markdown",
    author=["Cyprien", "Impact"],
    author_email=["cyprien.alexandre@ird.fr", "pascal.mouquet@ird.fr"],
    license="GPLv3+",
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Intended Audience :: Developers",
        "Intended Audience :: Education",
        "Intended Audience :: Science/Research",
        "Topic :: Scientific/Engineering :: GIS",
        "License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)",
        "Operating System :: Unix",
        "Programming Language :: Python",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.5",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
    ],
    keywords="GIS sentinel",  # Optional
    packages=find_packages(exclude=["dev", "example", "docs", "tests"]),
    install_requires=open("requirements.txt").read().splitlines(),
)
