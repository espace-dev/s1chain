#!/usr/bin/env python

import snappy
from snappy import ProductIO, GPF, HashMap
import os
import rasterio
import xml.etree.ElementTree as ET


try:
    HashMap = snappy.jpy.get_type('java.util.HashMap')
except:
    print('error JPY')

# Get snappy Operators
try:
    GPF.getDefaultInstance().getOperatorSpiRegistry().loadOperatorSpis()
except:
        print('error GPF')

input = os.getcwd()#'data'
output = os.getcwd()#'output'


def stacker(files):
    with rasterio.open(files[0]) as src0:
        meta = src0.meta
    meta.update(count = len(files))
    with rasterio.open('stack.tif', 'w', **meta) as dst:
        for id, layer in enumerate(files, start=1):
            with rasterio.open(layer) as src1:
                dst.write_band(id, src1.read(1))
    stacked = ProductIO.readProduct(os.path.join(input, 'stack.tif'))
    print('     DIM conversion')
    ProductIO.writeProduct(stacked,os.path.join(input, 'stacked.dim'),'BEAM-DIMAP')
    os.remove(os.path.join(input, 'stack.tif'))
    return stacked

def dim_edit():
    tree = ET.parse('stacked.dim')
    root = tree.getroot()
    for SBI in root.iter('Spectral_Band_Info'):
        unit = ET.SubElement(SBI, 'PHYSICAL_UNIT')
        unit.text= 'm'
        ET.dump(unit)
    tree.write('stacked.dim')
    edited_dim = ProductIO.readProduct(os.path.join(input, 'stacked.dim'))
    return edited_dim

def multitemp_despeckle(data):
    params = HashMap()
    params.put('sourceBandNames', 'band_1,band_2,band_3,band_4,band_5,band_6,band_7')
    params.put('anSize', '50')
    params.put('dampingFactor','2')
    params.put('enl', '1.0')
    params.put('estimateENL','true')
    params.put('filter', 'Lee Sigma')
    params.put('filterSizeX', '3')
    params.put('filterSizeY', '3')
    params.put('numLooksStr', '1')
    params.put('sigmaStr', '0.9')
    params.put('windowSize', '7x7')

    speckle = GPF.createProduct('Multi-Temporal-Speckle-Filter', params, data)
    ProductIO.writeProduct(speckle, os.path.join(input, 'spk.tif'), 'GeoTIFF-BigTIFF')
    return speckle

def process(files):
    # data = ProductIO.readProduct(os.path.join(input, file))
    # get the end date from the file name and get first 8 substring as YYYYmmdd
    # datestamp = file.split('_')[5][:8]
    print('Stacking...')
    stack_images = stacker(files)
    print('...done')
    print('Editing dim...')
    edited = dim_edit()
    print('...done')
    print('despeckle...')
    speckle = multitemp_despeckle(edited)
    print('...done')
    print('finished')
    return True

def set_path():
    os.path.dirname(os.path.dirname(__file__))
    path = os.path.join(os.getcwd())
    os.chdir(path)
    return path

def main():
    path = set_path()
    files = [f for f in os.listdir(path) if f.endswith('.tif')]
    process(files)
    # for file in files:
        # status = process(file)

if __name__ == '__main__':
    main()