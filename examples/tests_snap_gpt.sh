#!/bin/bash

# changer le chemin en fonction de l'instal de snap
cd ~/logiciels/snap/bin/


# pour faire la stack à partir d'un dossier
# problème sur l'ordre des fichiers, peut-etre donner plutôt une liste cf. ligne ci-dessous
./gpt  ~/s1chain/snap_xml/01_create_stack.xml -Pfilelist="/DATA_S2/TEMP/PM/Test_S1/input_files/" -Poutfile="/DATA_S2/TEMP/PM/Test_S1/stack.dim" 

# pour faire la stack à partir d'une liste de fichiers
./gpt  ~/s1chain/snap_xml/01_create_stack.xml -Pfilelist=\
"/DATA_S2/TEMP/PM/Test_S1/input_files/s1a_18RTQ_vv_DES_011_20190805t110955.tif,"\
"/DATA_S2/TEMP/PM/Test_S1/input_files/s1a_18RTQ_vv_DES_011_20190817t110956.tif,"\
"/DATA_S2/TEMP/PM/Test_S1/input_files/s1a_18RTQ_vv_DES_011_20190829t110957.tif,"\
"/DATA_S2/TEMP/PM/Test_S1/input_files/s1a_18RTQ_vv_DES_011_20190910t110952.tif,"\
"/DATA_S2/TEMP/PM/Test_S1/input_files/s1a_18RTQ_vv_DES_113_20190905txxxxxx.tif,"\
"/DATA_S2/TEMP/PM/Test_S1/input_files/s1b_18RTQ_vv_DES_011_20190904t110909.tif,"\
"/DATA_S2/TEMP/PM/Test_S1/input_files/s1b_18RTQ_vv_DES_113_20190911txxxxxx.tif" \
-Poutfile="/DATA_S2/TEMP/PM/Test_S1/stack.dim" 

# insérer ici le modif du dimap (python)

# pour faire le filtrage temporel
./gpt  ~/s1chain/snap_xml/02_filter.xml -Pinput="/DATA_S2/TEMP/PM/Test_S1/stack.dim" -Poutfile="/DATA_S2/TEMP/PM/Test_S1/filter.dim" 

# eclater le stack filtered

