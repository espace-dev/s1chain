#!/usr/bin/python3
# -*- coding: utf-8 -*-

from s1chain import DownloadProcess


dl_instruction = DownloadProcess('38KND',
									'2019-09-01',
									'2019-10-01'
									)

#~ print(dl_instruction.S1Processor_path)
dl_instruction.update_config()
dl_instruction.s1tiling()
