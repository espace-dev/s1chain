#!/usr/bin/python3
# -*- coding: utf-8 -*-

from s1chain import ndr_processing

images = ["/DATA_S2/S1_CHAIN/S1_TILED/38KND/filtered/s1a_38KND_vh_DES_166_20171212txxxxxx_filtered.tif",
			"/DATA_S2/S1_CHAIN/S1_TILED/38KND/filtered/s1a_38KND_vh_DES_166_20180117txxxxxx_filtered.tif",
			"/DATA_S2/S1_CHAIN/S1_TILED/38KND/filtered/s1a_38KND_vh_DES_166_20180129txxxxxx_filtered.tif"
			]

ndr_instruction = ndr_processing(images,'38KND','vh','DES')

print(ndr_instruction.pol)
print(ndr_instruction.path_list)
ndr_instruction.refNDR()
