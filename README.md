# S1Chain

Sentinel 1 processing chain, from download to flood detection.

**More infos :**
[https://framagit.org/espace-dev/s1chain/-/wikis/home](https://framagit.org/espace-dev/s1chain/-/wikis/home)

## Installation :
```
cd s1chain
pip3 install .
```

## S1Library usage :
```
>>> from s1chain import S1Library
>>> S1Library().tiles
['17RQK', '45QTB', '45QUB', '18RTP', '38KND', '30SXH', '18RTN', '36KXD', '30UVU', '18RTQ', '45QUC']
```
```
>>> S1Library()._38KND
['s1a_38KND_vv_DES_166_20171212txxxxxx.tif', 's1a_38KND_vh_DES_166_20190205txxxxxx.tif', 's1b_38KND_vh_DES_166_20180111t022651.tif',
's1a_38KND_vh_ASC_086_20180111t152619.tif', 's1a_38KND_vh_ASC_159_20180116txxxxxx.tif', 's1a_38KND_vv_ASC_159_20180116txxxxxx.tif',
's1a_38KND_vh_DES_166_20171212txxxxxx.tif', 's1b_38KND_vh_ASC_086_20180117txxxxxx.tif', 's1a_38KND_vv_ASC_086_20180111t152619.tif',
's1b_38KND_vv_DES_166_20180111t022651.tif', 's1a_38KND_vv_DES_166_20180129txxxxxx.tif', 's1a_38KND_vh_DES_166_20180129txxxxxx.tif',
's1b_38KND_vv_ASC_086_20180117txxxxxx.tif', 's1a_38KND_vv_DES_166_20190205txxxxxx.tif', 's1a_38KND_vv_DES_166_20180117txxxxxx.tif',
's1a_38KND_vh_DES_166_20180117txxxxxx.tif']
```
Filtering products:\
Use `filter_attr` function with a list of string keywords `['s1a', 's1b', ASC', 'DES', 'vv', 'vh']`
```
>>> S1Library()._45QTB.filter_attr(['DES', 'vv'])
['s1a_45QTB_vv_DES_121_20190509txxxxxx.tif', 's1a_45QTB_vv_DES_019_20190502t002133.tif', 's1a_45QTB_vv_DES_019_20190526txxxxxx.tif', 
's1a_45QTB_vv_DES_019_20190420txxxxxx.tif', 's1a_45QTB_vv_DES_019_20190514txxxxxx.tif', 's1a_45QTB_vv_DES_121_20190427txxxxxx.tif']
```

Filtering dates:\
Use `filter_dates` function with a start and end dates in format `'YYYY-MM-DD'`. Filters can be chained.
```
>>> S1Library()._45QTB.filter_dates(start_date = '20190401', end_date = '20190510')
['s1a_45QTB_vv_DES_121_20190509txxxxxx.tif', 's1a_45QTB_vv_DES_019_20190502t002133.tif', 's1a_45QTB_vv_DES_019_20190420txxxxxx.tif', 
's1a_45QTB_vh_DES_121_20190427txxxxxx.tif', 's1a_45QTB_vh_DES_121_20190509txxxxxx.tif', 's1a_45QTB_vh_DES_019_20190502t002133.tif', 
's1b_45QTB_vv_ASC_158_20190505txxxxxx.tif', 's1a_45QTB_vh_DES_019_20190420txxxxxx.tif', 's1b_45QTB_vh_ASC_158_20190505txxxxxx.tif', 
's1a_45QTB_vv_DES_121_20190427txxxxxx.tif']
>>>
>>> S1Library()._45QTB.filter_dates('20190401', '20190510').filter_attr(['ASC'])
['s1b_45QTB_vv_ASC_158_20190505txxxxxx.tif', 's1b_45QTB_vh_ASC_158_20190505txxxxxx.tif']

```
Filtering nodata:\
Use `filter_nodata` on tiled products with the minimum percentage of image needed.
```
>>> S1Library()._17RQK.tiled.filter_nodata(80)
```


## DownloadProcess usage :
### Initialize library
```
from s1chain import DownloadProcess
dl_instruction = DownloadProcess([all parameters necessary])
```
### Update S1-Tiling config file
```
dl_instruction.update_config()
```
### Launch download and process images
```
dl_instruction.s1tiling()
```

